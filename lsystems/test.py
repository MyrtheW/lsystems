import lsystems as ls
import visualisation
import rule_generation
import random

sl = ls.SimpleLSystem(["A", "B", "C", "D", "E", "(", ")", " "], {})
sl.addRule("A", "(BC")
sl.addRule("B", "D E")
sl.addRule("C", "E)")
sl.addRule("D", "A")
sl.addRule("E", "C B")
sl.addRule("(", "(")
sl.addRule(")", ")")
sl.addRule(" ", " ")

lc = ls.LineCollector(sl, axiom="A", indexing_priority="center-left")
for _ in range(9):
    lc.applyToPreviousAndPush()
#lc.printTreeVisualisation()

colmap = visualisation.randomColorMap(sl.symbols, 120, 333)
filename = "./test/vis.png"
visualisation.drawLinesToGrid(filename, lc, colmap)

for _ in range(25):
    seed = int(random.random() * 50000)
    print(seed)
    symb, prod = rule_generation.generateSimpleSystem(["A", "B", "C", "D", "E"], [4], seed, len_target=1)
    print(prod)
    for s in prod.keys():
        st = prod[s]
        prod[s] = ls.addSpacersToString(".", st, space_all=True)
    print(prod)

    sys = ls.SimpleLSystem(symb, prod)
    lc = ls.LineCollector(sys, axiom="A", indexing_priority="center-left")
    while True:
        nl = lc.lsystem.applyToString(lc.lines[-1])
        if len(nl) >= 1000:
            break
        else:
            lc.pushLine(nl, lc.computeIndex(nl))
        if len(lc.lines) >= 100:
            print("Max iterations reached")
            break
        
    filename = "./test/t" + str(seed) + ".png"
    try:
        visualisation.drawLinesToGrid(filename, lc, colmap, background_color=(0,0,0))
    except ValueError:
        print("Out of bounds for seed")
